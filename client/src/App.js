import logo from './logo.svg';
import './App.css';
import {
  Container,
  Button,
  Table,
  Card,
  CardMedia,
}
  from "@material-ui/core"

function App() {
  return (
    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.js</code> and save to reload.
    //     </p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn React
    //     </a>
    //   </header>
    // </div>

    <Container>
      <Card>
        <Container>
          <h1 className="app">This is header</h1>
          <img src={logo} alt="logo" />
        </Container>
      </Card>
      <Card>
      </Card>
      <Button color="secondary">Hello World</Button>
    </Container>
  );
}

export default App;
